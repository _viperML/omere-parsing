import pandas as pd
from enum import Enum


class DoseFields(Enum):
    thickness = "Thickness Al (mm)"
    total_dose = "Total dose (krad)"


class LetFields(Enum):
    let = "LET (MeV * cm^2 * mg^-1)"
    flux = "Flux (cm-2.s-1)"


def parse_dose(path: str) -> pd.DataFrame:
    with open(path, encoding="utf-8", errors="ignore") as f:
        result = pd.read_csv(f, delim_whitespace=True, comment="#", header=None)

    result.columns = [
        DoseFields.thickness,
        "Trapped electrons (rad)",
        "Trapped protons (rad)",
        "Solar proton (rad)",
        "Other electrons (rad)",
        "Other protons (rad)",
        "Gamma photons (rad)",
        "Other gamma photons (rad)",
        DoseFields.total_dose
    ]

    # Units
    result[DoseFields.total_dose] = result[DoseFields.total_dose]/1000

    return result


def parse_let(path: str) -> pd.DataFrame:
    with open(path, encoding="utf-8", errors="ignore") as f:
        result = pd.read_csv(f, delim_whitespace=True, comment="#", header=None)

    result.columns = [
        LetFields.let,
        LetFields.flux,
        "Differential flux"
    ]

    # Units
    result[LetFields.let] = result[LetFields.let]/1000

    return result


def pretty_save(fig, path: str):
    fig.savefig(path, format="jpg", dpi=600, bbox_inches='tight')


if __name__ == '__main__':
    pass
