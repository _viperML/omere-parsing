{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    flake-parts.url = "github:hercules-ci/flake-parts";
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    ...
  }:
    inputs.flake-parts.lib.mkFlake {inherit self;} {
      systems = [
        "x86_64-linux"
        "aarch64-linux"
      ];

      perSystem = {pkgs, ...}: {
        devShells.default = with pkgs;
          mkShellNoCC {
            packages = [
              (python3.withPackages (p: [
                p.jupyter
                p.pandas
                p.ipykernel
                p.black
                p.flake8
                p.matplotlib
              ]))
              asciidoctor-with-extensions
            ];
            shellHook = ''
              venv="$(cd $(dirname $(which python)); cd ..; pwd)"
              ln -Tsf "$venv" .venv
            '';
          };
      };
    };
}
